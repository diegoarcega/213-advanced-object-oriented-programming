/**
* A linked list of Tunes.
* The operations are:
* add a tune, like a tune, list all the tunes, list alphabetically and list by likes
*
* @author Diego Casas de Arcega
* Student ID: 201027179
*/
public class TuneList{

   /**
   * The main pointer to the list of tunes
   *
   */
   private Node theList;

   /**
   * Creates a new <code>TuneList</code> instance
   * Initialises the TuneList instance to null
   */
   public TuneList()
   {
    theList = null;
   }

   /**
   * Adds a Tune in the list of Nodes
   * The Tunes are ordered alphabetically by default
   * @param artist is the artist name of the Tune
   * @param title is the title of the Tune
   * <ul>
   *    <li> Situation #1: The TuneList is null, thus the Tune is inserted in the head of the list.</li>
   *	  <li> Situation #2: The TuneList already has a Tune with the same artist and title.</li>
   *	  <li> Situation #3: The Tune added is not alphabetically higher than the head of the TuneList.</li>
   *	  <li> Situation #4: The Tune added is alphabetically higher than the head of the TuneList. </li>
   * </ul>
	 */
   public void addTune(String artist, String title)
   {
    	 /**
       * Creates the Tune having the artist and the title of the Tune
       * @param artist, title of the Tune to be added
       */
       Tune t = new Tune(artist,title);

  	  /**
      * Situation #1: The TuneList is null, thus the Tune is inserted in the head of the list.
      *
      */
      if(theList == null){
          theList = new Node(t, theList);
          return;
      }

    	/**
      * Situation #2: The TuneList already has a Tune with the same artist and title.
      *
      */

      /**
      * Create a temporary TuneList only to loop throught it.
      * Compare whether the Tune added is equal to the head of the existing TuneList.
      * If they are equal, return and do not add the Tune to the TuneList
      */
    	Node tempo = theList;
    	while(tempo!=null){
    		if(t.getSong().equals(tempo.head().getSong()) ) {
    			System.out.println(t.getSong() + " Tune is already in the list.");
    			return;
    		}
    		tempo = tempo.tail();
    	}

    	/**
      * Situation #3: The tune inserted is not alphabetically higher than the head of the list.
      *
      */
    	if (t.getSong().compareTo(theList.head().getSong()) > 0 ) {

      /**
      * Put a Tune inside a Node
      * Create a temporary pointer to loop through the TuneList
      */
  		Node new_tune = new Node(t);
  		Node n = theList;
      /**
      * Find a Tune in the TuneList that is greater than the Tune to be inserted.
      * If it finds, stop the pointer inside the temporary TuneList and add the tail to be the new tune.
      */
      while(n.tail()!=null && (new_tune.head().getSong().compareTo(n.tail().head().getSong()) > 0)){
  			n = n.tail();
  		}
  		if (n.tail() == null){
  			 n.setTail(new_tune);
  		}else{
  			/**
        * If the Tune to be inserted is lesser than one Tune in the TuneList but higher than other
        * Put the Tune in the TuneList between them other Nodes.
        */
        Node temp = n.tail();
  			n.setTail(new_tune);
  			new_tune.setTail(temp);
  		}
  		return;
      }

  	/**
    * Situation #4: The tune inserted is alphabetically higher than the head of the list.
    *
    */
  	if (t.getSong().compareTo(theList.head().getSong()) < 0 ) {
      /**
      * Create a new Node, with the new Tune being the first element
      * Set the Tail of this new Node to be the previous TuneList
      * Make the theList pointer point to this new list.
      */
      Node new_tune = new Node(t);
  		new_tune.setTail(theList);
  		theList = new_tune;
  		return;
      }


   }


   /**
   * Utilizes the Tune artist and the Tune title to search inside the list for the Tune to be liked
   * Loop through the TuneList comparing artist and title with the head of each Node with the params given
   * @param artist name of the artist or band to be liked
   * @param title  of the song to be liked
   */
   public void likeTune(String artist, String title){
        Node n = theList;
        while(n !=null){
          if (n.head().getSong().equals(artist+title)) {
            n.head().like();
            return;
          }else{
             n = n.tail();
          }
        }
   }


   /**
   *
   * Loops through the TuneList applying the .toString() method to each Node within
   * @return Generates a String with the Tune Artist and Tune Title of every Node in the TuneList
   */
   public String listAll(){
        Node n = theList;
        String listAllSt = "";
        while(n != null){
            listAllSt += n.head().toString();
            n = n.tail();
        }
        return listAllSt;
    }

    /**
    * The TuneList is already sorted alphabetically by default
    * @return The String generated from the listAll() method
    */
    public String listAlphabetically(){
    	return listAll();
    }

     public String listByLikes(){
      return listAll();
    }


}