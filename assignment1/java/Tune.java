/**
* A Tune to be wrapped by the Node and then inserted in the TuneList
* Contains the artist, title and number of likes it receives from the users
* @author Diego Casas de Arcega
* Student ID: 201027179
*/

public class Tune{
	/**
	* the artist of the Tune
	*
	*/
	private String artist;
	/**
	* the title of the Tune
	*
	*/
	private String title;
	/**
	* the amount of likes the Tune has
	*
	*/
	private int likes;

	/**
	* Create a new Tune by giving its artist and title
	* The number of likes is 0 by default when a new Tune is instantiated
	*/
	public Tune(String _artist, String _title){
		this.artist = _artist;
		this.title = _title;
		this.likes = 0;
	}

	/**
	* changes de name of the artist
	* @param name of the artist of the Tune
	*/
	public void setArtist(String _artist){
		this.artist = _artist;
	}

	/**
	* Get the Tune's artist
	* @return name of the artist or band
	*/
	public String getArtist(){
		return this.artist;
	}

	/**
	* changes de title of the Tune
	* @param title the Tune
	*/
	public void setTitle(String _title){
		this.title = _title;
	}

	/**
	* Get the Tune's title
	* @return title of the Tune
	*/
	public String getTitle(){
		return this.title;
	}

	/**
	* Gets the number of the Tune's likes
	* @return number of likes a Tune received
	*/
	public int getLikes(){
		return this.likes;
	}

	/**
	* Increases the number of likes in a Tune
	*
	*/
	public void like(){
		this.likes = likes + 1;
	}

	/**
	* Generate a string with all the properties in the tune
	* @return the whole data within the Tune
	*/
	public String toString() {
        return  this.artist + "\n" + this.title + "\n" + this.likes + "\n";
   }

   /**
	* Helper method for comparison between the new Tune and the exiting Tune in the TuneList
	* @return Tune Artist and Tune title together
	*/
   public String getSong(){
		return this.getArtist() + this.getTitle();
   }

}