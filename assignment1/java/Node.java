/**
* Node of the list.
* It has Tunes inside the head pointer
* @author Diego Casas de Arcega
*/

public class Node
{
   /**
   * Points to the element in the list
   *
   */
   private Tune head;

   /**
   * Points to the list after the head
   *
   */
   private Node tail;

   /**
   * Creates a new instance of the Node
   * head points to the first element
   * @param the Tune to be inserted
   */
   public Node(Tune t)
   {
      head = t;
      tail = null;

   }

   /**
   * In case of a existing TuneList, put it as the tail and the new Tune as the first element
   * @param t new Tune to be added
   * @param _tail  existing list to serve as the tail
   */
   public Node(Tune t, Node _tail)
   {
      head = t;
      tail = _tail;
   }

   /**
   * Get the head of the TuneList
   * @return the Tune in the Node
   */
   public Tune head()
   {
      return this.head;
   }

   /**
   * Changes the head of the Node
   * @param t the Tune to serve as the new head of the Tune
   */
   public void setHead(Tune t){
		this.head = t;
   }

   /**
   * Changes the tail pointer of the Node
   * @param n Node to serve as the tail for the current Node
   */
   public void setTail(Node n){
		this.tail = n;
   }

   /**
   * Get the tail of the Node
   * @return the Node that serves as tail to the current Node
   */
   public Node tail()
   {
      return tail;
   }

}