/* A few general points from the first assignment that are relevant
to the second:

- make sure your code layout is consistent;
  this is a really easy way of getting a couple of marks

- javadoc comments aren't required for the second assignment,
  but still there should be comments for each class and each
  class member.  Also, each variable you declare should have
  a comment to say what it's for, and if a block of code does
  something that can be summarised in a few words, it's worth
  putting that into a comment.  Code without comments will
  generally get low marks.

 - this won't affect the marks, but you'll have noticed from your
  print-outs that it's *much* easier to read code that sticks to
  the standard page-width of 80 characters.

and a few points about the second assignment:

- remember your server handles only *one* request per session:
  your session-handling code shouldn't have a loop to read
  multiple requests from a client.

- the tests you submit don't need to be extensive: about ten
  or so tests should cover the important cases.

- critical sections should be big enough to protect the shared
  resource from interference, and small enough to allow other
  threads to get on with their tasks if possible.  you can't really
  test your server for thread-safety because what happens is
  under the control of the time-slicer when the program runs.*/
import java.io.*;
import java.net.*;


public class LikeTunesServer{
  public static void main(String[] args){
    try{
      ServerSocket ss = new ServerSocket(12015);
      Socket incoming = ss.accept();

      BufferedReader in = new BufferedReader(new InputStreamReader(incoming.getInputStream()));
      PrintWriter out = new PrintWriter(new OutputStreamWriter(incoming.getOutputStream()));
      TuneList tuneList = new TuneList();

      String line;
      String artist;
      String title;
      boolean done = false;

      out.println(">>> Hello visitor,\nYou have the following options:" +
        "\n(0) Add a Tune\n(1) Like a Tune\n(2) listAlphabetically \n(3) listByLikes\n\n[bye] to exit the program.");
      out.flush();

      while(!done){
        line = in.readLine();

        if((line == null) || (line.trim().equals("bye"))) done = true; //else out.println("Echo: " + line); out.flush();
        else{

         switch(line){
            case "0" : out.println("->You chose:\n->Add a Tune(0)"); out.flush();
            out.println("->Please, inform the artist" ); out.flush();
            artist = in.readLine();
            if(artist.trim().equals("")) out.println("> 5" ); out.flush();
            out.println("->Now, please inform the title" ); out.flush();
            title = in.readLine();
            if(title.trim().equals("")) out.println("> 5" ); out.flush();
            tuneList.addTune(artist,title);
            break;

            case "1" : out.println("--> You chose:\n Like a Tune(0)");
            out.println(">Please, inform the artist" ); out.flush();
            artist = in.readLine();
            if(artist.trim().equals("")) out.println("5" ); out.flush();
            out.println(">Now, please inform the title" ); out.flush();
            title = in.readLine();
            if(title.trim().equals("")) out.println("5" ); out.flush();
            tuneList.likeTune(artist,title);
            break;

            case "2" : out.println("----> View all tunes in alphabetical order <----");
            out.println(tuneList.listAlphabetically());
            out.println("----------------------------"); out.flush();
            break;

            case "3" : out.println("----> View all tunes ordered by popularity <----");
            out.println(tuneList.listByLikes());
            out.println("----------------------------"); out.flush();
            break;

            case "\n" : out.println("5"); out.flush(); //didnt know how to treat this.
            break;

            default :  out.println("7"); out.flush();
            break;
          }
           out.println("You have the following options:" +
          "\n(0) Add a Tune\n(1) Like a Tune\n(2) listAlphabetically \n(3) listByLikes\n\n[bye] to exit the program.");
          out.flush();
        }
      }

      out.flush();
      incoming.close();
      ss.close();
    }catch(Exception e){
      e.printStackTrace();
    }

  }
}