
import java.io.*;
import java.net.*;


public class LikeTunesServer{

  // Port number for the server
  private  static final int PORT_NUMBER = 12015;

  // Server Socket
  private static  ServerSocket ss;

  //Set when shutDown() is called to stop the server;
  private static boolean shutDownCalled = false;

  // Tune list that will go to the server
  public static TuneList tuneList = new TuneList();

  //Shut the server down, closing the server socket
  public static void shutDown(){
	//flag to close  server socket
	shutDownCalled =  true;
	try{
		ss.close();
	}catch(Exception e){
		// problem shutting down the server socket
		System.err.println("Problem shutting down the server socket");
		System.err.println(e.getMessage());
		System.exit(1);
	}
  }

  public static void main(String[] args){

	//for client connections
	Socket incoming;

	//Session-handling thread
	Thread t;

	try{
		//set up server socket
		ss = new ServerSocket(PORT_NUMBER);
		while(true){
			incoming = ss.accept();

			//start session-handler in new thread
			t = new Thread(new TunesHandler(incoming, tuneList));
			t.start();
		}
	}catch(SocketException se){
		if(!shutDownCalled){
			// problem in server socket
			System.err.println("Problem in the socket");
			System.err.println(se.getMessage());
			System.exit(1);
		}
	}catch(IOException ioe){
    // problem in server socket
    System.err.println("6 problem in server socket");
    System.err.println(ioe.getMessage());
    System.exit(1);

	}finally{
    if(ss != null){
      try{ss.close();}catch(Exception e){ System.err.println("Closing: " + e.getMessage());}
    }
  }
}
}
class TunesHandler implements Runnable{
  //Connection to the client
  private Socket client;
  private TuneList tuneList;

  TunesHandler(Socket s, TuneList t){
    client = s;
    tuneList = t;
  }

  /* Handle session for one remote client
  *  Set up I/O until client sends "bye"
  */
  public void run(){
    // Input/Output
    BufferedReader in = null;
    PrintWriter out = null;

    try{
      // Prepare I/O
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));

      // vars where the input will be stored before being added to the tunelist
      String artist;
      String title;

      // Addressing client input
      String line;
      boolean done = false;

      // Welcome message
      out.println("---------------------------------------------------");
      out.println("--> Hello visitor\nYou have the following options:" +
      "\n(0) Add a Tune\n(1) Like a Tune\n(2) listAlphabetically \n(3) listByLikes\n[bye] to exit the program.");
      out.flush();

      while (!done){
        line = in.readLine();
        if((line == null) || (line.trim().equals("bye"))) done = true;
        else{
         switch(line){
            case "0" : out.println("->You chose:\n->Add a Tune"); out.flush();
             out.println("->Please, inform the artist" ); out.flush();
            artist = in.readLine();
            if(artist.trim().equals("")) out.println("5" ); out.flush();
             out.println("->Now, please inform the title" ); out.flush();
            title = in.readLine();
            if(title.trim().equals("")) out.println("5" ); out.flush();
            // only Adds a valid tune, with a proper title and artist
            if(!artist.trim().equals("") || !title.trim().equals("") ) tuneList.addTune(artist,title);
            break;

            case "1" : out.println("-> You chose:\n->Like a Tune");
            out.println("->Please, inform the artist" ); out.flush();
            artist = in.readLine();
            if(artist.trim().equals("")) out.println("5" ); out.flush();
            out.println("->Now, please inform the title" ); out.flush();
            title = in.readLine();
            if(title.trim().equals("")) out.println("5" ); out.flush();
            tuneList.likeTune(artist,title);
            break;

            case "2" : out.println("----> View all tunes in alphabetical order <----");
            out.println(tuneList.listAlphabetically());
            out.println("------------------------------------"); out.flush();
            break;

            case "3" : out.println("----> View all tunes ordered by popularity <----");
            out.println(tuneList.listByLikes());
            out.println("------------------------------------"); out.flush();
            break;

            //Unrecognised request: if the client sends a request that does not begin with "0", "1", "2", or"3", the server responds by sending a single line consisting of "7".
            default :  out.println("7"); out.flush();
            break;
          }
        }

        try{in.close();}catch(IOException e){ System.err.println("6");}
        if(out != null){ out.close();}
        if(client != null){ try{client.close();}catch(IOException e){ System.err.println("6"); }}
      }

    }catch(Exception e){
      // Fatal I/O error for this session
      System.err.println("6");
      System.err.println(e.getMessage());
    }finally{
      //close connections
      try{in.close();}catch(IOException e){ System.err.println("6 in.close();");}
      if(out != null){ out.close();}
      if(client != null){ try{client.close();}catch(IOException e){ System.err.println("6 client.close();"); }}

    }
  }
}

