import java.io.*;
import java.net.*;

public class LikeTunesServerTest{

  public static void main(String[] args){
    Socket client;
    BufferedReader in = null;
    PrintWriter out = null;
    String line;

    try{
      /*
      *  Test 1 - Adding Tunes Option [0]
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 1 - Adding Tunes Option [0]==============");
      client = new Socket("localhost", 12015);
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("0");  // add tune
      out.flush();
      out.println("Trwbador");
      out.flush();
      out.println("Red Handkerciefs");
      out.flush();

      client = new Socket("localhost", 12015);
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("0");  // add tune
      out.flush();
      out.println("Ariana Grande");
      out.flush();
      out.println("Problem");
      out.flush();

      client = new Socket("localhost", 12015);
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("0");  // add tune
      out.flush();
      out.println("Clean Bandit");
      out.flush();
      out.println("Rather Be");
      out.flush();

      client = new Socket("localhost", 12015);
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("0");  // add tune
      out.flush();
      out.println("Ed Sheeran");
      out.flush();
      out.println("Kiss me");
      out.flush();
      client.close();

      client = new Socket("localhost", 12015);
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("0");  // add tune
      out.flush();
      out.println("Oasis");
      out.flush();
      out.println("Wonderwall");
      out.flush();
       while ((line = in.readLine()) != null) {
          System.out.println(line);
       }
      client.close();

      /*
      *  Test #2 - Like a Tune
      * Option [1]
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 2 - Like Tunes Option [1]==============");
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("1");  // like tune
      out.flush();
      out.println("Trwbador");
      out.flush();
      out.println("Red Handkerciefs");
      out.flush();
       while ((line = in.readLine()) != null) {
          System.out.println(line);
       }
      client.close();

      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader
                      (new InputStreamReader(client.getInputStream()));
      out = new PrintWriter
                      (new OutputStreamWriter(client.getOutputStream()));
      out.println("1");  // like tune
      out.flush();
      out.println("Oasis");
      out.flush();
      out.println("Wonderwall");
      out.flush();
       while ((line = in.readLine()) != null) {
          System.out.println(line);
       }
      client.close();

      /*
      *  Test #3 - List all tunes in alphabetical order
      * Option [2]
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 3 - List all tunes in alphabetical order, Option [2]==============");
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
      out.println("2"); // request all tunes in alphabetical order
      out.flush();
      while ((line = in.readLine()) != null) {
         System.out.println(line);
      }
      client.close();

      /*
      *  Test #4 - List all tunes in order of popularity
      * Option [3]
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 3 - List all tunes in order of popularity, Option [3]==============");
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
      out.println("3"); // request all tunes in order of popularity
      out.flush();
      while ((line = in.readLine()) != null) {
         System.out.println(line);
      }
      client.close();

      /*
      *  Test #4 - [bye] to exit the program
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 4 - [bye] to exit the program, Option [bye]==============");
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
      out.println("bye");
      out.flush();
      while ((line = in.readLine()) != null) {
         System.out.println(line);
      }
      client.close();

      /*
      *  Test #5 - [a] to throw the error number 7
      * Unrecognised request: if the client sends a request that does not begin with "0", "1", "2", or"3", the server responds by sending a single line consisting of "7".
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 5 - Unrecognised request==============");
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
      out.println("a");
      out.flush();
      while ((line = in.readLine()) != null) {
         System.out.println(line);
      }
      client.close();

      /*
      * Test #6 - Empty line - ADDING A TUNE
      * If the client sends an empty line ("\n") for either the artist or the title of a tune that the client is attempting to add or like, the server responds by sending a single line consisting of "5".
      */
      System.out.println("=========================================================");
      System.out.println("===========Test 6 - Empty line==============");
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
      out.println("0");
      out.flush();
      out.println(" ");
      out.flush();
      out.println(" ");
      out.flush();
      while ((line = in.readLine()) != null) {
         System.out.println(line);
      }
      client.close();

      /*
      * Test #7 - Empty line - LIKE A TUNE
      * If the client sends an empty line ("\n") for either the artist or the title of a tune that the client is attempting to add or like, the server responds by sending a single line consisting of "5".
      */
      client = new Socket("localhost", 12015);
      line = null;
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
      out.println("1");
      out.flush();
      out.println(" ");
      out.flush();
      out.println(" ");
      out.flush();
      while ((line = in.readLine()) != null) {
         System.out.println(line);
      }
      client.close();



    }catch(Exception e){
      System.err.println(e.getMessage());
    }
  }
}
